\documentclass[a4paper,10pt]{scrartcl}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{upgreek}

\usepackage{fullpage}

\usepackage{graphicx}
\usepackage[usenames]{color}
\usepackage[labelformat=simple]{subcaption}

\usepackage{siunitx}
\usepackage{bm}

\usepackage{dirtree}

\usepackage[backend=biber,sorting=none,citestyle=ieee]{biblatex}
\addbibresource{bibliography.bib}

\usepackage{calc}
\input{custom_commands.tex}



% Title Page
\title{Platform for prototyping MPC algorithms for motion control with PMSM motors}
\author{Lukáš Černý \and Zdeněk Hurák}



\begin{document}
\maketitle



\begin{abstract}
In this report we describe a setup for fast prototyping of \textit{model predictive control} (MPC) algorithms for motion control with \textit{permament magnet synchronous motors} (PMSM). In particular, we focus on the methods of \textit{finite set model predictive control} (FS-MPC), which impose quite some stringent implementation requirements on the hardware and software. The intended use is \textit{hybrid position and force control} in robotics. We give both detailed instructions on how to use the setup and comments on some experiments already conducted with this setup.  
\end{abstract}




\section{Introduction and motivation}

% PMSM advantages
PMSMs are often used in both industrial applications and robotic projects in academia.
The reasons are their advantages over other types of motors.
Namely, the advantages include absence of commutator (which ensures energy efficiency, mechanical simplicity, reliability), relatively high torque-to-volume ratio, and the ability of field weakening.

% Classical control and MPC
Classical and well-estabilished control methods for PMSMs include six-step operation, hysteresis direct torque control, and primarily field-oriented PID control.
MPC for PMSM is an alternative that, based on a mathematical model of PMSM with constraints taken into account, finds optimal control inputs over a finite time horizon.
There are in fact two different types of MPC for PMSMs.
The first is continuous-control-set MPC (CCS-MPC), which assumes the control inputs to be from a continuous interval, for example [0, 24 V].
The switching nature of the inverter is neglected and the actual control inputs are only approximations of the controller outputs obtained by pulse-width-modulation (PWM).
The second is finite-control-set MPC (FCS-MPC), which takes the switching nature into consideration and generates the switching signals for the inverter directly, which can be used to optimize the switching losses.
To control velocity and position, both MPC algorithms can be designed either as a single controller in a centralized structure, or as a cascaded connection of multiple controllers.

% MPC challenges
The implementation of the MPC algorithms is however challenging and computationally demanding.
The reason is the fast dynamics of PMSMs, which requires sampling frequency of several tens of kilohertz in the case of CCS-MPC, and ten up to hundred times more in the case of FCS-MPC.
The required sampling frequency is determined mostly by PMSM electrical time constant and DC-bus voltage of the inverter power supply (or a DC link).
Therefore, the MPC algorithms for PMSMs are usually designed with a short prediction horizon.
Specifically, FCS-MPC for PMSMs is almost exclusively designed with prediction horizon of one or two.

% Goal
The goal of this report is to describe the two MPC algorithms, their possible hardware implementation, and the hardware platform used to test the implementation.
It should also serve as an instruction manual to anyone who wants to start working with the platform in KN:E-8.
Also, it presents data obtained by simulation and real-world experiments.



\section{Modeling permanent magnet synchronous motors (PMSM)}
In this chapter, a brief description of the mathematical model is summarized.
More details are in \cite{LCThesis}.

\begin{figure}[tb]
	\begin{center}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/pmsm_model_star/pmsm_model_star.pdf}
  			\subcaption{Star configuration. \label{fig:PMSM_Model_Star}}
		\end{subfigure}
		\hspace{0.1\textwidth}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
 	 		\includegraphics[width=0.95\textwidth]{figs/pmsm_model_delta/pmsm_model_delta.pdf}
 	 		\subcaption{Delta configuration. \label{fig:PMSM_Model_Delta}}
		\end{subfigure}
	  	\caption{Model of electrical part of PMSM (the voltage arrows are oriented toward the reference point).\label{fig:PMSM_Model}}
	\end{center}
\end{figure}


\subsection{ABC model}
The model if delta and star configurationis in fig. \ref{fig:PMSM_Model}.
It is described by
\begin{alignat}{1}
	\bm{L}\derivative{\bm{i}_{\textrm{abc}}}{t} \meq
	- R_{\textrm{s}}\bm{i}_{\textrm{abc}} 
	- \omega\bm{L}^{\prime}\bm{i}_{\textrm{abc}}
	- \bm{e}_{\textrm{abc}}
	+ \bm{v}_{\textrm{abc}}, \\
	J\derivative{\omega}{t} \meq \frac{1}{2}\bm{i}_{\textrm{abc}}\transpose\bm{L}^{\prime}\bm{i}_{\textrm{abc}} + \frac{1}{\omega}\bm{i}_{\textrm{abc}}\transpose\bm{e}_{\textrm{abc}} - B\omega + \tau_{\textrm{cog}} + \tau_{\textrm{ex}}, \\
	\derivative{\theta}{t} \meq \omega
\end{alignat}
where

\begin{alignat}{1}
	\bm{v}_{\textrm{abc}}  \meq 
	\begin{bmatrix}
		v_{\textrm{a}} \cr v_{\textrm{b}} \cr v_{\textrm{c}}
	\end{bmatrix}, \quad
	\bm{i}_{\textrm{abc}} \meqna
	\begin{bmatrix}
		i_{\textrm{a}} \cr i_{\textrm{b}} \cr i_{\textrm{c}}
	\end{bmatrix}, \quad
	\bm{e}_{\textrm{abc}} \meqna 
	\begin{bmatrix}
		e_{\textrm{a}} \cr e_{\textrm{b}} \cr e_{\textrm{c}}
	\end{bmatrix} \meqna
	 -n_{\textrm{pp}}\omega\mathit{\Psi}_{\textrm{m}}
	\begin{bmatrix}
		\sin\braRound{n_{\textrm{pp}}\theta} \cr
		\sin\braRound{n_{\textrm{pp}}\theta - \frac{2\pi}{3}} \cr
		\sin\braRound{n_{\textrm{pp}}\theta + \frac{2\pi}{3}}
	\end{bmatrix}, \\
	\bm{L} \meq \begin{bmatrix}
		 L_{\textrm{s}} & -M_{\textrm{s}} & -M_{\textrm{s}} \cr
	   	-M_{\textrm{s}} &  L_{\textrm{s}} & -M_{\textrm{s}} \cr
	    -M_{\textrm{s}} & -M_{\textrm{s}} &  L_{\textrm{s}}
	\end{bmatrix} + L_{\textrm{g}}\begin{bmatrix}
		\cos2\braRound{n_{\textrm{pp}}\theta} &
		\cos2\braRound{n_{\textrm{pp}}\theta - \frac{\pi}{3}} &
		\cos2\braRound{n_{\textrm{pp}}\theta + \frac{\pi}{3}} \cr
		\cos2\braRound{n_{\textrm{pp}}\theta - \frac{\pi}{3}} &
		\cos2\braRound{n_{\textrm{pp}}\theta - \frac{2\pi}{3}} &
		\cos2\braRound{n_{\textrm{pp}}\theta} \cr
		\cos2\braRound{n_{\textrm{pp}}\theta + \frac{\pi}{3}} &
		\cos2\braRound{n_{\textrm{pp}}\theta} &
		\cos2\braRound{n_{\textrm{pp}}\theta + \frac{2\pi}{3}}
	\end{bmatrix},\\
	\bm{L}^{\prime} \meq \pderivative{\bm{L}}{\theta}.
\end{alignat}


\subsection{Two-phase equivalent model}
Delta and star connected PMSM model is transformed to a two-phase equivalent model using the reduced Clarke transformation
\begin{alignat}{2}
	\tilde{\bm{K}} \mdef \frac{2}{3}\begin{bmatrix}
		1 & -\frac{1}{2} & -\frac{1}{2} \cr
		0 & \frac{\sqrt{3}}{2} & - \frac{\sqrt{3}}{2}
	\end{bmatrix}, \quad 
	\tilde{\bm{K}}^{-1} \mdef \frac{2}{3}\begin{bmatrix}
		1 & 0 \cr
		-\frac{1}{2} & \frac{\sqrt{3}}{2} \cr
		-\frac{1}{2} & -\frac{\sqrt{3}}{2} 
	\end{bmatrix}.
\end{alignat}
The model is then
\begin{alignat}{1}
	\bm{L}_{\upalpha\upbeta}\derivative{\bm{i}_{\upalpha\upbeta}}{t} \meq
	- R_{\textrm{s}}\bm{i}_{\upalpha\upbeta} 
	- \omega \bm{L}_{\upalpha\upbeta}^{\prime}\bm{i}_{\upalpha\upbeta}
	- \bm{e}_{\upalpha\upbeta}
	+ \bm{v}_{\upalpha\upbeta}, \\
	J\derivative{\omega}{t} \meq 
	\frac{3}{4}\bm{i}_{\upalpha\upbeta}\transpose 
	\bm{L}_{\upalpha\upbeta}^{\prime}\bm{i}_{\upalpha\upbeta}
	+ \frac{3}{2\omega}\bm{i}_{\upalpha\upbeta}\transpose
	\bm{e}_{\upalpha\upbeta} 
	- B\omega + \tau_{\textrm{cog}} + \tau_{\textrm{ex}}, \\
	\derivative{\theta}{t} \meq \omega,
\end{alignat}
where 
\begin{alignat}{2}
	\bm{i}_{\upalpha\upbeta} \meq \begin{bmatrix}
		i_{\upalpha} & i_{{\upbeta}}
	\end{bmatrix}\transpose \meq \tilde{\bm{K}} \bm{i}_{\textrm{abc}}, \\
	\bm{v}_{\upalpha\upbeta} \meq \begin{bmatrix}
		v_{\upalpha} & v_{{\upbeta}}
	\end{bmatrix}\transpose \meq \tilde{\bm{K}} \bm{v}_{\textrm{abc}}, \\
	\bm{e}_{\upalpha\upbeta} \meq \begin{bmatrix}
		e_{\upalpha} & e_{{\upbeta}}
	\end{bmatrix}\transpose \meq \tilde{\bm{K}} \bm{e}_{\textrm{abc}},
\end{alignat}
and
\begin{alignat}{1}
    \bm{L}_{\upalpha\upbeta} \meq
    \begin{bmatrix}
    	L_{\textrm{s}} + M_{\textrm{s}} & 0 \cr
    	0 & L_{\textrm{s}} + M_{\textrm{s}}
    \end{bmatrix} + \frac{3}{2}L_{\textrm{g}} 
    \begin{bmatrix}
         \cos (2n_{\textrm{pp}}\theta) &
         \sin (2n_{\textrm{pp}}\theta) \cr
         \sin (2n_{\textrm{pp}}\theta) &
         -\cos (2n_{\textrm{pp}}\theta)
    \end{bmatrix}, \\
    \bm{L}_{\upalpha\upbeta}^{\prime} \meq 
	3 n_{\textrm{pp}} L_{\textrm{g}}
    \begin{bmatrix}
        -\sin (2n_{\textrm{pp}}\theta) &
        \cos (2n_{\textrm{pp}}\theta) \cr
        \cos (2n_{\textrm{pp}}\theta) &
        \sin (2n_{\textrm{pp}}\theta)
    \end{bmatrix}, \\
    \bm{e}_{\upalpha\upbeta} \meq
     - n_{\textrm{pp}} \omega \mathit{\Psi}_{\textrm{m}}
    \begin{bmatrix}
        \sin(n_{\textrm{pp}} \theta) &
        -\cos(n_{\textrm{pp}} \theta)
    \end{bmatrix}\transpose.
\end{alignat}


\subsection{DQ model -- matrix form}
The model is further transformed to the rotating frame of reference using the Park transformation
\begin{alignat}{2}
	\tilde{\bm{R}}\braRound{\varphi} \mdef \begin{bmatrix}
	 \cos\braRound{\varphi} & \sin\braRound{\varphi}\\
	-\sin\braRound{\varphi} & \cos\braRound{\varphi}
	\end{bmatrix}, \quad
	\tilde{\bm{R}}^{-1}\braRound{\varphi} \mdef \begin{bmatrix}
	 \cos\braRound{\varphi} & -\sin\braRound{\varphi}\\
	 \sin\braRound{\varphi} &  \cos\braRound{\varphi}
	\end{bmatrix}.
\end{alignat}
The model is then

\begin{alignat}{1}
	\bm{L}_{\textrm{dq}}\derivative{\bm{i}_{\textrm{dq}}}{t} \meq
	- R_{\textrm{s}}\bm{i}_{\textrm{dq}} 
	- \omega \dtilde{\bm{L}}_{\textrm{dq}}\bm{i}_{\textrm{dq}}
	- \bm{e}_{\textrm{dq}}
	+ \bm{v}_{\textrm{dq}}, \\
	J\derivative{\omega}{t} \meq 
	\frac{3}{4}\bm{i}_{\textrm{dq}}\transpose 
	\tilde{\bm{L}}_{\textrm{dq}}\bm{i}_{\textrm{dq}}
	+ \frac{3}{2\omega}\bm{i}_{\textrm{dq}}\transpose
	\bm{e}_{\textrm{dq}} - B\omega + \tau_{\textrm{cog}} + \tau_{\textrm{ex}}, \\
	\derivative{\theta}{t} \meq \omega,
\end{alignat}
where 
\begin{alignat}{2}
	\bm{i}_{\textrm{dq}} \meq \begin{bmatrix}
		i_{\textrm{d}} & i_{{\textrm{q}}}
	\end{bmatrix}\transpose \meq \tilde{\bm{R}}\braRound{n_{\textrm{pp}}\theta} \bm{i}_{\upalpha\upbeta}, \\
	\bm{v}_{\textrm{dq}} \meq \begin{bmatrix}
		v_{\textrm{d}} & v_{{\textrm{q}}}
	\end{bmatrix}\transpose \meq \tilde{\bm{R}}\braRound{n_{\textrm{pp}}\theta} \bm{v}_{\upalpha\upbeta}, \\
	\bm{e}_{\textrm{dq}} \meq \begin{bmatrix}
		e_{\textrm{d}} & e_{{\textrm{q}}}
	\end{bmatrix}\transpose \meq \tilde{\bm{R}}\braRound{n_{\textrm{pp}}\theta} \bm{e}_{\upalpha\upbeta},
\end{alignat}
and
\begingroup
\allowdisplaybreaks
\begin{alignat}{1}
	\bm{L}_{\textrm{dq}} \meq
	\begin{bmatrix}
        L_{\textrm{s}} + M_{\textrm{s}} + \frac{3}{2} L_{\textrm{g}} &
        0 \cr
        0 &
        L_{\textrm{s}} + M_{\textrm{s}} - \frac{3}{2} L_{\textrm{g}}
    \end{bmatrix}, \\
	\tilde{\bm{L}}_{\textrm{dq}} \meq
	3 n_{\textrm{pp}}	
	\begin{bmatrix}
        0 &
        L_{\textrm{g}}  \cr
        L_{\textrm{g}}  &
        0
    \end{bmatrix}, \\
	\dtilde{\bm{L}}_{\textrm{dq}} \meq
	n_{\textrm{pp}}	
	\begin{bmatrix}
        0 &
        -\braRound{L_{\textrm{s}} + M_{\textrm{s}} - \frac{3}{2}L_{\textrm{g}}} \cr
        L_{\textrm{s}} + M_{\textrm{s}} + \frac{3}{2} L_{\textrm{g}} &
        0
    \end{bmatrix} \\
	\bm{e}_{\textrm{dq}} \meq
	n_{\textrm{pp}}	\omega \mathit{\Psi}_{\textrm{m}}
	\begin{bmatrix}
        0 & 1
    \end{bmatrix}\transpose.
\end{alignat}


\subsection{DQ model -- scalar form}
The DQ model in scalar form is
\begin{alignat}{1}
	L_{\textrm{d}}\derivative{i_{\textrm{d}}}{t} \meq 
	- R_{\textrm{s}}i_{\textrm{d}}
	+ n_{\textrm{pp}}\omega L_{\textrm{q}}i_{\textrm{q}}
	+ v_{\textrm{d}}, \\
	L_{\textrm{q}}\derivative{i_{\textrm{q}}}{t} \meq 
	- R_{\textrm{s}}i_{\textrm{q}}
	- n_{\textrm{pp}}\omega L_{\textrm{d}}i_{\textrm{d}}
	- K_{\textrm{b}}\omega
	+ v_{\textrm{q}}, \\
	J\derivative{\omega}{t} \meq
	\frac{3}{2}n_{\textrm{pp}}i_{\textrm{d}}i_{\textrm{q}}(L_{\textrm{d}} - L_{\textrm{q}})
	+ \frac{3}{2} K_{\textrm{b}}i_{\textrm{q}}
	- B\omega + \tau_{\textrm{cog}} + \tau_{\textrm{ex}}, \\
	\derivative{\theta}{t} \meq \omega,
\end{alignat}
where
\begin{alignat}{1}
	L_{\textrm{d}} \meq L_{\textrm{s}} + M_{\textrm{s}} + \frac{3}{2} L_{\textrm{g}},\\
	L_{\textrm{q}} \meq L_{\textrm{s}} + M_{\textrm{s}} - \frac{3}{2} L_{\textrm{g}},\\ 
	K_{\textrm{b}} \meq n_{\textrm{pp}}	\mathit{\Psi}_{\textrm{m}}. 
\end{alignat}


\subsection{Linearized model}
For control purposes, the DQ model of PMSM is linearized into the following standard state-space form
\begin{alignat}{1}
	\dot{\bm{x}} \meq \bm{A}_{\textrm{c}}\bm{x} + \bm{B}_{\textrm{c}}\bm{u}, \\
	\bm{y} \meq \bm{C}_{\textrm{c}}\bm{x} + \bm{D}_{\textrm{c}}\bm{u},
\end{alignat}
where the state vector and input vector are
\begin{alignat}{1}
	\bm{x} \meq \begin{bmatrix}
		i_{\textrm{d}} &
		i_{\textrm{q}} &
		\omega &
		\theta
	\end{bmatrix}\transpose, \\
	\bm{u} \meq \begin{bmatrix}
		v_{\textrm{d}} &
		v_{\textrm{q}}
	\end{bmatrix}\transpose.
\end{alignat}
The cogging torque and the external torque are not considered in this model.
The linear DQ model is obtained by making a two assumptions and approximations, which simplify the nonlinear model into a linear one.
Firstly, the reluctance torque is neglected as it is usually small compared to the synchronous torque.
Secondly, it is assumed that $\omega$ does not change too much during controller sampling period, or over the prediction horizon in case of MPC.
This justifies one to consider $\omega$ a constant parameter in the nonlinear terms $n_{\textrm{pp}}\omega L_{\textrm{q}}i_{\textrm{q}}$ and $n_{\textrm{pp}}\omega L_{\textrm{d}}i_{\textrm{d}}$.
This gives linear time-varying model 
\begin{alignat}{2}
	\bm{A}_{\textrm{c}} \meq \begin{bmatrix}
		-\dfrac{R_{\textrm{s}}}{L_{\textrm{d}}} &
		 n_{\textrm{pp}}\omega \dfrac{L_{\textrm{q}}}{L_{\textrm{d}}} &
		 0 &
		 0 \\[1.3ex]
		-n_{\textrm{pp}}\omega \dfrac{L_{\textrm{d}}}{L_{\textrm{q}}} &
		-\dfrac{R_{\textrm{s}}}{L_{\textrm{q}}} &
		 -\dfrac{K_{\textrm{b}}}{L_{\textrm{q}}} &
		 0 \\[1.3ex]
		 0 &
		 \dfrac{3}{2}\dfrac{K_{\textrm{b}}}{J} &
		 -\dfrac{B}{J} &
		 0 \\[1.3ex]
		 0 & 0 & 1 & 0
	\end{bmatrix}, \quad
	\bm{B}_{\textrm{c}} \meq \begin{bmatrix}
		\dfrac{1}{L_{\textrm{d}}} & 0 \\[1.3ex]
		0 & \dfrac{1}{L_{\textrm{q}}} \\[1.3ex]
		0 & 0 \\[1.3ex]
		0 & 0
	\end{bmatrix} %\label{eq:PMSM_Linear_B}
\end{alignat}
parametrized by $\omega$.
Matrix $\bm{D}_{\textrm{c}} = \bm{0}$ is zero matrix and $\bm{C}_{\textrm{c}}$ is chosen based on the control objective. 
Such linearized model was also used in \cite{BeldaLinearModel}.



\subsection{Discretization}
Discrete-time linear model of PMSM with sampling period $T_{\textrm{s}}$
\begin{alignat}{1}
	\bm{x}_{k+1} \meq \bm{A}_{\textrm{d}}\bm{x}_{k} + \bm{B}_{\textrm{d}}\bm{u}_{k}, \\
	\bm{y}_{k} \meq \bm{C}_{\textrm{d}}\bm{x}_{k} + \bm{D}_{\textrm{d}}\bm{u}_{k}
\end{alignat}
is obtained from the continuous-time model assuming constant inputs over the sampling period, which results in
\begin{alignat}{1}
	\bm{A}_{\textrm{d}} \meq e^{T_{\textrm{s}}\bm{A}_{\textrm{c}}}, \\
	\bm{B}_{\textrm{d}} \meq \braRound{\int\limits_{\tau=0}^{\tau=T_{\textrm{s}}}e^{\tau\bm{A}_{\textrm{c}}}\diff{\tau}}\bm{B}_{\textrm{c}}, \\
	\bm{C}_{\textrm{d}} \meq \bm{C}_{\textrm{c}}, \\
	\bm{D}_{\textrm{d}} \meq \bm{D}_{\textrm{c}}.
\end{alignat}
When the discrete-time state-space matrices are to be computed online within a limited amount of time, Euler approximation
\begin{alignat}{1}
	\bm{A}_{\textrm{d}} \mapprox \bm{I} + T_{\textrm{s}}\bm{A}_{\textrm{c}}, \\
	\bm{B}_{\textrm{d}} \mapprox T_{\textrm{s}}\bm{B}_{\textrm{c}}
\end{alignat}
can be used.


\subsection{Constraints}

The current constraints in the DQ variables are approximated by
\begin{alignat}{1}
	i_{\textrm{d}}^2 + i_{\textrm{q}}^2 \mleq I_{\textrm{max}}^2.
\end{alignat}
This nonlinear constraint is further approximated by set of linear constraints.
Specifically, octagon is used as the approximated feasible region in a similar way as in \cite{CiminiMPCOnlinePMSM}.
The octagon is in fig.~\ref{fig:PMSMConstraintsCDQ} and is described by
\begin{alignat}{1}
	\bm{P}_{x}\bm{x} \mleq \bm{p}_{x},
\end{alignat}
where
\begingroup
\renewcommand*{\arraystretch}{0.75}
\begin{alignat}{2}
	\bm{P}_{x} \meq \begin{bmatrix}
		      +1 &   +\sqrt{2}-1 & 0 & 0 \cr
              +1 &   -\sqrt{2}+1 & 0 & 0 \cr
              -1 &   +\sqrt{2}-1 & 0 & 0 \cr
              -1 &   -\sqrt{2}+1 & 0 & 0 \cr
     +\sqrt{2}-1 &            +1 & 0 & 0 \cr
     +\sqrt{2}-1 &            -1 & 0 & 0 \cr
     -\sqrt{2}+1 &            +1 & 0 & 0 \cr
     -\sqrt{2}+1 &            -1 & 0 & 0
	\end{bmatrix}, \quad
	\bm{p}_{x} = \begin{bmatrix}
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}} \cr
		I_{\textrm{max}}
	\end{bmatrix}.
\end{alignat}

The voltage constraints in the DQ variables are
\begin{alignat}{1}
	v_{\textrm{d}}^2 + v_{\textrm{q}}^2 \mleq V_{\textrm{max}}^2.
\end{alignat}
This nonlinear constraint is again approximated by octagon
\begin{alignat}{1}
	\bm{P}_{u}\bm{u} \mleq \bm{p}_{u},
\end{alignat}
where
\begingroup
\renewcommand*{\arraystretch}{0.75}
\begin{alignat}{2}
	\bm{P}_{u} \meq \begin{bmatrix}
		      +1 &   +\sqrt{2}-1 \cr
              +1 &   -\sqrt{2}+1 \cr
              -1 &   +\sqrt{2}-1 \cr
              -1 &   -\sqrt{2}+1 \cr
     +\sqrt{2}-1 &            +1 \cr
     +\sqrt{2}-1 &            -1 \cr
     -\sqrt{2}+1 &            +1 \cr
     -\sqrt{2}+1 &            -1
	\end{bmatrix}, \quad
	\bm{p}_{u} = \begin{bmatrix}
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}} \cr
		V_{\textrm{max}}
	\end{bmatrix}.  \label{eq:PMSM_constraint_u}
\end{alignat}
\endgroup
The octagon is in fig.~\ref{fig:PMSMConstraintsVDQ}.

% Figure
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.49\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/constraints/constraints3.pdf}
  			\subcaption{Aprroximated constraints on DQ currents. \label{fig:PMSMConstraintsCDQ}}
		\end{subfigure}
		\hspace{0.02\textwidth}
		\begin{subfigure}[b]{0.49\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/constraints/constraints4.pdf}
 	 		\subcaption{Approximated constraints on DQ voltages. \label{fig:PMSMConstraintsVDQ}}
		\end{subfigure}		
	  	\caption{Current and voltage constraints transformed to DQ variables.\label{fig:PMSMConstraints}}
	%\end{center}
\end{figure}


\section{Field oriented PI controller}

\section{Continuous-control-set model predictive control (CCS-MPC)}


\subsection{Sparse form}
CCSMPC formulates the control task as an optimization problem.
The following formulation of convex quadratic program with linear constraints was chosen
\begin{subequations}
\label{eq:MPC_Sparse_Form}
\begin{alignat}{3}
	\min\limits_{\bm{z}}\quad &  & 
	& \hspace{-4.2em}\sum\limits_{k = 0}^{N-1}\;   (\bm{y}_{k+1} - \bm{r}_{k+1})\transpose\bm{Q}(\bm{y}_{k+1} - \bm{r}_{k+1}) + \Delta{\bm{u}_k\transpose}\bm{R}\Delta{\bm{u}_k} \\
	\textrm{s.t.}\quad & &
	\bm{x}_{k+1} \meq \bm{A}\bm{x}_k + \bm{B}\bm{u}_k,\\ & &
	\bm{y}_{k+1} \meq \bm{C}\bm{x}_{k+1},\\ & &
	\bm{P}_{x}\bm{x}_{k+1} \mleq \bm{p}_{x},\\ & &
	\bm{P}_{u}\bm{u}_{k} \mleq \bm{p}_{u},\\ & &
	\bm{z} \meq \begin{bmatrix}
		{\bm{u}_0}\transpose & {\bm{u}_1}\transpose & \dots & {\bm{u}_{N-1}\transpose}		
	\end{bmatrix}\transpose, \\ & &
	\Delta\bm{u}_k \meq \bm{u}_k - \bm{u}_{k-1}, \\ & &
	\bm{x}_0 \meq \bm{x}(t),\\ & &
	\bm{u}_{-1} \meq \bm{u}(t - T_{\textrm{s}}),\\ & &
	\bm{r}_{k+1} \meq \bm{r}(t + k T_{\textrm{s}}),
\end{alignat}
\end{subequations}
where weighting matrix $\bm{Q}$ is symmetric and positive semidefinite, weighting matrix $\bm{R}$ is symmetric and positive definite, and $N$ is the prediction horizon.
In this formulation, the control horizon is the same as prediction horizon.
The controller operates with sampling period $T_{\textrm{s}}$ and the control law is as follows.
At time $t$, state $x(t)$ is measured and system matrix $\bm{A}$ is obtained by discretization of the linear continuous-time state-space matrix, which is parametrized by $\omega$.
Matrices $\bm{B}$ and $\bm{C}$ stay fixed.
The controller then finds optimal sequence $\bm{z}$, from which only $\Delta{\bm{u}_0}$ is taken to compute $\bm{u}_0 = \bm{u}_{-1} + \Delta\bm{u}_0$.
The voltage vector $\bm{u}_0$ is then applied to the motor and saved for the next sampling period.
If the reference output $\bm{r}$ is given for the next sampling period $\bm{r}(t + T_{\textrm{s}})$ only, constant reference over the whole prediction horizon is assumed, i.e. $\bm{r}_{k+1} = \bm{r}(t + T_{\textrm{s}})$.


\subsection{Condensed form}
The above formulation of the optimization problem can be expressed in condensed form, in which the predicted state is eliminated.
The condensed form is
\begin{subequations}
\label{eq:MPC_Condensed_Form}
\begin{alignat}{3}
	\min\limits_{\bm{z}}\quad &  & 
	& \hspace{-2.2em}\frac{1}{2} \bm{z}\transpose\bm{H}\bm{z} + \begin{bmatrix}
		\bm{x}_0\transpose & \bm{u}_{-1}\transpose & \bm{r}_1\transpose & \bm{r}_2\transpose & \dots & \bm{r}_N\transpose
	\end{bmatrix}\bm{F}\transpose\bm{z}\\
	\textrm{s.t.}\quad &  &
	\bm{G} \bm{z} \mleq \bm{W} + \bm{S}\bm{x}_0, \\ & &
	\bm{z} \meq \begin{bmatrix}
		{\bm{u}_0\transpose} & {\bm{u}_1\transpose} & \dots & {\bm{u}_{N-1\transpose}}		
	\end{bmatrix}\transpose,\\ & &
	\bm{x}_0 \meq \bm{x}(t),  \\ & &
	\bm{u}_{-1} \meq \bm{u}(t - T_{\textrm{s}}),\\ & &
	\bm{r}_{k} \meq \bm{r}(t + kT_{\textrm{s}}), \quad k \;=\; 1,\, 2,\, \dots, \, N,
\end{alignat}
\end{subequations}
where matrices $\bm{H}$, $\bm{F}$, $\bm{G}$, $\bm{W}$, $\bm{S}$ are derived in appendix \cite{LCThesis}.
To obtain solution, this formulation can be easily plugged into standard QP solvers.
Choice of matrix $\bm{C}$ determines reference outputs to be tracked.


\subsection{Dual QP of the condensed form}
Now we denote
\begin{alignat}{1}
	\bm{a} \meq \begin{bmatrix}
		\bm{x}_0\transpose & \bm{u}_{-1}\transpose & \bm{r}_1\transpose & \bm{r}_2\transpose & \dots & \bm{r}_N\transpose
	\end{bmatrix}\transpose.
\end{alignat}
The dual cost function of the condensed cost function is defined as
\begin{alignat}{2}
	q(\bm{y}) \meq \inf\limits_{\bm{z}} \mathcal{L}(\bm{z}, \bm{y}) \meq \inf\limits_{\bm{z}} \braCurl{\frac{1}{2} \bm{z}\transpose\bm{H}\bm{z} + \bm{a}\transpose\bm{F}\transpose\bm{z} + \bm{y}\transpose(\bm{G}\bm{z} - \bm{W} - \bm{S}\bm{x}_0)},
\end{alignat}
where $\bm{y}$ is the vector of Lagrange multipliers and is dual feasible if $\bm{y} \geq 0$.
Because $\bm{H}$ is positive deinite, the infimum is achieved for
\begin{alignat}{1}
	\pderivative{\mathcal{L}(\bm{z}, \bm{y})}{\bm{z}} \meq 0,
\end{alignat}
i.e.
\begin{alignat}{1}
	\bm{z} \meq -\bm{H}^{-1}(\bm{F}\bm{a} + \bm{G}\bm{y}).\label{eq:DualToPrimal}
\end{alignat}
Therefore, the dual cost function is 
\begin{alignat}{2}
	q(\bm{y}) \meq -\frac{1}{2}\bm{y}\transpose\bm{G}\bm{H}^{-1}\bm{G}\transpose\bm{y} - \braRound{\bm{W} + \bm{S}\bm{x}_0 + \bm{G}\bm{H}^{-1}\bm{F}\bm{a}}\transpose\bm{y} - \frac{1}{2}\bm{a}\transpose\bm{F}\transpose\bm{H}^{-1}\bm{F}\bm{a},
\end{alignat}
The dual problem is
\begin{alignat}{3}
	\max\limits_{\bm{y}}\quad &  & 
	& \hspace{-1em}q(\bm{y}) \\
	\textrm{s.t.}\quad & &
	\bm{y} \mgeq 0,
\end{alignat}
or
\begin{alignat}{3}
	\min\limits_{\bm{y}}\quad &  & 
	& \hspace{-1em} -q(\bm{y}) \\
	\textrm{s.t.}\quad & &
	\bm{y} \mgeq 0,
\end{alignat}
which is a convex QP.
The dual problem in this case satisfies strong duality, i.e. the optimal value of the dual problem equals the optimal value of the primal problem and the duality gap is zero.
This can be proven e.g. by showing that the Slater's condition is satisfied, i.e. that the problem is strictly feasible.
The dual problem is rewritten as
\begin{alignat}{3}
	\min\limits_{\bm{y}}\quad &  & 
	& \hspace{-1em} \frac{1}{2}\bm{y}\transpose\bm{Q}\bm{y} + \bm{d}\transpose\bm{y} \\
	\textrm{s.t.}\quad & &
	\bm{y} \mgeq 0,\\ & &
	\bm{Q} \meq \bm{G}\bm{H}^{-1}\bm{G}\transpose,\\ & &
	\bm{d} \meq \braRound{\bm{W} + \bm{S}\bm{x}_0 + \bm{G}\bm{H}^{-1}\bm{F}\bm{a}}.
\end{alignat}


\subsection{Solution to the dual QP}
We denote the dual cost function in the minimization problem as
\begin{alignat}{1}
	f(\bm{y}) \meq \frac{1}{2}\bm{y}\transpose\bm{Q}\bm{y} + \bm{d}\transpose\bm{y}.
\end{alignat}
It can be shown that
\begin{alignat}{2}
	\braNorm{\pderivative{f(\bm{x})}{\bm{x}} - \pderivative{f(\bm{y})}{\bm{y}}}_2 \meq \braNorm{\bm{Q}(\bm{x} - \bm{y})}_2 \mleq |\lambda_{\textrm{max}}(\bm{Q})|\braNorm{\bm{x} - \bm{y}}_2.
\end{alignat}
The idea behind fast proximal gradient method is to take 
\begin{alignat}{1}
	\lambda \mgeq \frac{1}{|\lambda_{\textrm{max}}(\bm{Q})|}
\end{alignat}
such that
\begin{alignat}{1}
	\bm{y}^{k+1} \meq \max\braCurl{\bm{y}^{k} - \lambda(\bm{Q}\bm{y}^{k} + \bm{d}), \, 0}
\end{alignat}
quickly converges to the minimum.
Due to the projection to the feasible set, $y^{k}$ at every iteration is dual feasible.
However, the choice of $\lambda$ is subtle and affects the rate of convergence.
The fast proximal gradient is as follows (todo).
The optimal solution to the primal QP is obtained from \eqref{eq:DualToPrimal}. 


\subsection{Single-loop v. multi-loop}



\section{Finite-control-set model predictive control (FCS-MPC)}
FCS-MPC formulates the control task almost identically as CCS-MPC:
\begin{subequations}
\label{eq:FCSMPC_Sparse_Form}
\begin{alignat}{3}
	\min\limits_{\bm{z}}\quad &  & 
	& \hspace{-4.2em}\sum\limits_{k = 0}^{N-1}\;   (\bm{y}_{k+1} - \bm{r}_{k+1})\transpose\bm{Q}(\bm{y}_{k+1} - \bm{r}_{k+1}) + \Delta{\bm{u}_k\transpose}\bm{R}\Delta{\bm{u}_k} \\
	\textrm{s.t.}\quad & &
	\bm{x}_{k+1} \meq \bm{A}\bm{x}_k + \bm{B}\bm{u}_k,\\ & &
	\bm{y}_{k+1} \meq \bm{C}\bm{x}_{k+1}, \\ & &
	\bm{P}_{x}\bm{x}_{k+1} \mleq \bm{p}_{x}, \\ & &
	\bm{u}_{k} \;&\in\;  \left\lbrace
		\begin{bmatrix}
			+\frac{1}{2} \\ +\frac{1}{2} \\ +\frac{1}{2}
		\end{bmatrix}, \,		
		\begin{bmatrix}
			+\frac{1}{2} \\ +\frac{1}{2} \\ -\frac{1}{2}
		\end{bmatrix}, \,			
		\begin{bmatrix}
			+\frac{1}{2} \\ -\frac{1}{2} \\ +\frac{1}{2}
		\end{bmatrix}	, \,		
		\begin{bmatrix}
			+\frac{1}{2} \\ -\frac{1}{2} \\ -\frac{1}{2}
		\end{bmatrix}		, \,	
		\begin{bmatrix}
			-\frac{1}{2} \\ +\frac{1}{2} \\ +\frac{1}{2}
		\end{bmatrix}			, \,
		\begin{bmatrix}
			-\frac{1}{2} \\ +\frac{1}{2} \\ -\frac{1}{2}
		\end{bmatrix}			, \,
		\begin{bmatrix}
			-\frac{1}{2} \\ -\frac{1}{2} \\ +\frac{1}{2}
		\end{bmatrix}			, \,
		\begin{bmatrix}
			-\frac{1}{2} \\ -\frac{1}{2} \\ -\frac{1}{2}
		\end{bmatrix}			
	\right\rbrace,\\ & &
	\bm{z} \meq \begin{bmatrix}
		{\bm{u}_0}\transpose & {\bm{u}_1}\transpose & \dots & {\bm{u}_{N-1}\transpose}		
	\end{bmatrix}\transpose,  \\ & &
	\Delta\bm{u}_k \meq \bm{u}_k - \bm{u}_{k-1},  \\ & &
	\bm{x}_0 \meq \bm{x}(t), \\ & &
	\bm{u}_{-1} \meq \bm{u}(t - T_{\textrm{s}}),  \\ & &
	\bm{r}_{k+1} \meq \bm{r}(t + k T_{\textrm{s}}).
\end{alignat}
\end{subequations}
The main difference is that constraints on $\bm{u}_{k}$ are replaced such that now $\bm{u}_{k}$ belongs to a finite set of vectors,
where each vector describes one switching combination in the inverter.
Each element of the vectors corresponds to one phase-leg in the inverter, where +1/2 means it is connected to $+v_{\textrm{dc}}/2$  and -1/2 means it is connected to  $-v_{\textrm{dc}}/2$.
Matrix $\bm{B}$ is therefore obtained by discretizing 
$\bm{B}_{\textrm{c}}\tilde{\bm{R}}\tilde{\bm{K}}v_{\textrm{dc}}$, which is the continuous-time input matrix multiplied by the reduced Park and Clarke transformation matrices and by DC-bus voltage.
In this formulation, the 2-norm squared $\braNorm{\Delta{\bm{u}_k}}_2^2$ is equal to the number of switching transitions that happen during step from time $k-1$ to $k$.
As there is usually no reason to penalize some switches of the inverter more than others, one usually chooses $\bm{R}=\lambda\bm{I}$, where $\lambda$ is a scalar weighting parameter.
This parameter penalizes the switching events and, thus, it can be used to tune the average steady-state switching frequency.
This problem can be solved by enumerating all possible sequence of the input vector.
However, number of such sequences is $8^N$, which makes the exhaustive search a feasible option only for very short prediction horizons.

FCS-MPC can also be expressed in a condensed form as
\begin{subequations}
\label{eq:FCSMPC_Condensed_Form}
\begin{alignat}{3}
	\min\limits_{\bm{z}}\quad &  & 
	& \hspace{-2.2em}\frac{1}{2} \bm{z}\transpose\bm{H}\bm{z} + \begin{bmatrix}
		\bm{x}_0\transpose & \bm{u}_{-1}\transpose & \bm{r}_1\transpose & \bm{r}_2\transpose & \dots & \bm{r}_N\transpose
	\end{bmatrix}\bm{F}\transpose\bm{z}\\
	\textrm{s.t.}\quad &  &
	\bm{G} \bm{z} \mleq \bm{W} + \bm{S}\bm{x}_0, \\ & &
	\bm{z} \meq \begin{bmatrix}
		{\bm{u}_0\transpose} & {\bm{u}_1\transpose} & \dots & {\bm{u}_{N-1\transpose}}		
	\end{bmatrix}\transpose,\\ & &
	\bm{u}_{k} \;&\in\;\mathbb{U}, \\ & &
	\bm{x}_0 \meq \bm{x}(t),  \\ & &
	\bm{u}_{-1} \meq \bm{u}(t - T_{\textrm{s}}),\\ & &
	\bm{r}_{k} \meq \bm{r}(t + kT_{\textrm{s}}), \quad k \;=\; 1,\, 2,\, \dots, \, N,
\end{alignat}
\end{subequations}
where the definition of some matrices slightly differs from CCS-MPC.
In \cite{GeyerBook}, the following formulation is ... (todo)

%\subsection{Control design}

%\subsubsection{Classical control}

%\subsubsection{MPC}

%\subsection{Numerical simulation}

\section{Simulink simulation designs}
\section{Prototyping setup}
For hardware implemenattion, AD-FMCMOTCON2-EBZ was chosen as
a suitable commercially available platform for PMSM control.

\subsection{Description of hardware}

For hardware implementation, AD-FMCMOTCON2-EBZ driver by Analog devices together with ZedBoard was chosen.
AD-FMCMOTCON2-EBZ is a driver system allowing control of two motors simultaneously.
It contains sigma-delta modulators for measuring the phase currents with 16-bit resolution at \SI{78.1}{\kilo\hertz}.
To measure at higher frequency, the resolution has to be decreased by decreasing decimation ration of demodulating filters.
ZedBoard is a prototyping platform with Zynq-7000 SoC, which contains ARM$^{\circledR}$  dual  Cortex$^{\circledR}$-A9 and Xilinx 7-series FPGA fabric on a single chip.
Both the ARM$^{\circledR}$ processing system  and FPGA fabric are supported by MATLAB and Simulink, and one can relatively easily implement co-designs both parts of the chip.
A photograph of the platform is shown in fig. \ref{fig:HardwarePlatform}.

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/foto/foto.jpg}
		\caption{ZedBoard and AD-FMCMOTCON2-EBZ with two coupled PMSM.  \label{fig:HardwarePlatform}}
	\end{center}
\end{figure}

\subsection{Descrition of software}
In order to run the Simulink control designs on the ZedBoard, the following software is reqired.
\begin{itemize}
	\item Xilinx Vivado -- any version compatible with MATLAB HDL Coder should work after minor changes to the design. The designs were created using Xilinx Vivado 2019.2 HL WebPACK (free license)
	\item MATLAB, Simulink, HDL Coder, Embedded Coder (available within the student MATLAB license)
	\item The following MATLAB add-ons:
	\begin{itemize}
		\item MinGW-w64 (MATLAB Support for MinGW-w64 C/C++ Compiler)
		\item Embedded Coder Support Package for Xilinx Zynq Platform
		\item HDL Coder Support Package for Xilinx Zynq Platform
	\end{itemize}

\end{itemize}


\subsection{Descrition of created simulink control designs}
There are five Simulink control designs, which are
\begin{itemize}
	\item pmsm\_pi -- field-oriented PI control
	\item pmsm\_ccsmpc\_sl -- CCS-MPC single-loop control
	\item pmsm\_ccsmpc\_ml -- CCS-MPC multi-loop control
	\item pmsm\_fcsmpc\_ss -- FCS-MPC single-step control (N = 1)
	\item pmsm\_fcsmpc\_ms -- FCS-MPC multi-step control (N = 3)
	\item pmsm\_fcsmpc\_ms\_ip -- FCS-MPC multi-step control (N = 3) implemented in VHDL with Simulink interface
\end{itemize}

Each of the designs is in its own directory and consists of multiple files. In all cases the files are almost the same.
\dirtree{%
.1 simulink\_design.
.2 pmsm\_pl.slx.
.2 pmsm\_ps.slx.
.2 init.m.
.2 sources.
.3 libs.
.4 HDLReferenceDesigns.
.4 pmsm\_lc\_lib.slx.
}

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/screenshots/SimulinkPL.png}
		\caption{pmsm\_pl.slx}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/screenshots/SimulinkPS.png}
		\caption{pmsm\_ps.slx}
	\end{center}
\end{figure}


\section{*Prototyping setup -- customized inverter}
In order to improve the hardware design, the inverter desgn was customized. (todo)


\begin{figure}[!h]
	\begin{center}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_LV_Board1.png}
		\end{subfigure}
		\hspace{0.1\textwidth}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_LV_Board2.png}
		\end{subfigure}
	  	\caption{LV board}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_MZ_Split_Board1.png}
		\end{subfigure}
		\hspace{0.1\textwidth}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_MZ_Split_Board2.png}
		\end{subfigure}
	  	\caption{MZ split board}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_ZB_Split_Board1.png}
		\end{subfigure}
		\hspace{0.1\textwidth}
		\begin{subfigure}[b]{0.44\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/custom_pcbs/PMSM_Driver_ZB_Split_Board2.png}
		\end{subfigure}
	  	\caption{ZB split board}
	\end{center}
\end{figure}

\section{Experiments}

\subsection{Instructions for running the experiments}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/screenshots/SimulinkTopBarHDL.png}
		\caption{Top bar in pmsm\_pl.slx}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/screenshots/SimulinkHDLWA.png}
		\caption{Running HDL Workflow Advisor in pmsm\_pl.slx}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{figs/screenshots/SimulinkTopBar.png}
		\caption{Top bar in pmsm\_ps.slx}
	\end{center}
\end{figure}

\subsection{Some conducted experiments}

\section{Conclusions}

\subsection{What was achieved}

\subsection{What remains to do}
\begin{itemize}
	\item Automatic calibration/identification.
	\item Better way to find zero angle (angle offset).
	\item Field weakening MPC.
	\item Inspect the centralized structure, try to tune.
	\item Measure the switching losses.
	\item Kalman filter instead of observers.
	\item Better current sensors.
\end{itemize}



\printbibliography


\end{document}          
